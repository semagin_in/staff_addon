<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    if ($mode == 'update') {
        $person_id = fn_staff_update_person($_REQUEST['person_data'], $_REQUEST['person_id'], DESCR_SL);
        $suffix = ".update?person_id=$person_id";
    }
    if ($mode == 'delete') {
        if (!empty($_REQUEST['person_id'])) {
            fn_delete_person_by_id($_REQUEST['person_id']);
        }
        $suffix = '.manage';
    }
    return array(CONTROLLER_STATUS_OK, 'staff_info' . $suffix);
}

if($mode == 'manage') {
    $params = $_REQUEST;
    $staff = fn_staff_get_data(DESCR_SL);
    Tygh::$app['view']->assign(array('staff' => $staff));
} elseif ($mode == 'update' || $mode == 'add') {
    $person_id = isset($_REQUEST['person_id'])
        ? $_REQUEST['person_id']
        : null;
    $person = $person_id
        ? fn_get_person_data($person_id)
        : [];
    Tygh::$app['view']->assign('person', $person);
}