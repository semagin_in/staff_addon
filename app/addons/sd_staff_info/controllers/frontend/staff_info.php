<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    return true;
}

if($mode == 'list') {
    $staff = fn_staff_get_data(DESCR_SL, false);
    Tygh::$app['view']->assign(array('staff' => $staff));
} elseif ($mode == 'person') {
    $person_id = isset($_REQUEST['person_id'])
        ? $_REQUEST['person_id']
        : null;
    $person = $person_id
        ? fn_get_person_data($person_id)
        : [];
    Tygh::$app['view']->assign('person', $person);
}