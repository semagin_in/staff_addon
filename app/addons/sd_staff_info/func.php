<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
use Tygh\Languages\Languages;

function fn_staff_get_data($lang_code, $edit_mode=true)
{
    if ($edit_mode) {
        $staff = db_get_array(
            "SELECT ?:staff.*, ?:staff_data.*, ?:staff_images.staff_image_id FROM ?:staff LEFT JOIN ?:staff_data ON ?:staff.staff_id=?:staff_data.staff_id LEFT JOIN ?:staff_images ON ?:staff.staff_id = ?:staff_images.staff_id WHERE ?:staff_data.lang_code=?s", $lang_code
        );
    } else {
        $staff = db_get_array(
            "SELECT ?:staff.*, ?:staff_data.*, ?:staff_images.staff_image_id FROM ?:staff LEFT JOIN ?:staff_data ON ?:staff.staff_id=?:staff_data.staff_id LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id WHERE ?:staff_data.lang_code=?s AND ?:staff.status = 'A'", $lang_code
        );
    }
    foreach ($staff as $staff_id => $person ) {

        $staff[$staff_id]['main_pair'] = fn_get_image_pairs($staff[$staff_id]['staff_image_id'], 'foto', 'M', true, false);
    }
    return $staff;

}
function fn_staff_update_person($data, $person_id, $lang_code = DESCR_SL)
{
    if (!empty($person_id)) {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $person_id);
        db_query("REPLACE INTO ?:staff_data (`staff_id`, `title`, `description`, `lang_code`) VALUES( ?i, ?s, ?s, ?s);", $person_id, $data['title'], $data['description'], $lang_code);
        $staff_image_id = fn_get_staff_image_id($person_id);
        $staff_image_exist = !empty($staff_image_id);
        $image_is_update = fn_staff_need_image_update();
        if ($staff_image_exist && $image_is_update) {
            fn_delete_image_pairs($staff_image_id, 'foto');
            db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i", $person_id);
            $staff_image_exist = false;
        }
        if ($image_is_update && !$staff_image_exist) {
            $staff_image_id = db_query("INSERT INTO ?:staff_images (staff_id) VALUE(?i)", $person_id);
        }
        $pair_data = fn_attach_image_pairs('staff_main', 'foto', $staff_image_id);
        if (!$staff_image_exist) {
        }

    }else {
        $person_id = $data['person_id'] = db_query("REPLACE INTO ?:staff ?e", $data);
        db_query("REPLACE INTO ?:staff_data (`staff_id`, `title`, `description`, `lang_code`) VALUES( ?i, ?s, ?s, ?s);", $person_id, $data['title'], $data['description'], $lang_code);
        if (fn_staff_need_image_update()) {
            $staff_image_id = db_get_next_auto_increment_id('staff_images');
            $pair_data = fn_attach_image_pairs('staff_main', 'promo', $staff_image_id, $lang_code);
            if (!empty($pair_data)) {
                $data_staff_image = array(
                    'staff_image_id' => $staff_image_id,
                    'staff_id'       => $person_id
                );
                db_query("INSERT INTO ?:staff_images ?e", $data_staff_image);
            }
        }
    }
    return $person_id;      
}

function fn_get_person_data($person_id, $lang_code = DESCR_SL)
{
    $fields = $joins = array();
    $condition = '';
    $fields = array (
        '?:staff.staff_id',
        '?:staff.first_name',
        '?:staff.last_name',
        '?:staff.email',
        '?:staff.pos',
        '?:staff.status',
        '?:staff_data.title',
        '?:staff_data.description',
        '?:staff_data.lang_code',
        '?:staff_images.staff_image_id'
    );

    $joins[] = db_quote("LEFT JOIN ?:staff_data ON ?:staff.staff_id=?:staff_data.staff_id AND ?:staff_data.lang_code=?s", $lang_code);
    $joins[] = db_quote("LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staff.staff_id" );
    $condition = db_quote("WHERE ?:staff.staff_id=?i", $person_id);
    
    $person_data = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:staff " . implode(" ", $joins) ." $condition");
    if (!empty($person_data['staff_image_id'])) {
        $person_data['main_pair'] = fn_get_image_pairs($person_data['staff_image_id'], 'foto', 'M', true, false);
    }
    if (!empty($person_data['staff_id'])) {
        return ($person_data['status']=='A') ? $person_data : [];
    }
    return false;
}

function fn_delete_person_by_id ($person_id)
{
     if (!empty($person_id)) {
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $person_id);
        db_query("DELETE FROM ?:staff_data WHERE staff_id = ?i", $person_id);
        $staff_images_ids = db_get_fields("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $person_id);
        foreach ($staff_images_ids as $staff_image_id) {
            fn_delete_image_pairs($staff_image_id, 'foto');
        }
        db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i", $person_id);
    }
}

function fn_get_staff_image_id($person_id)
{
    return db_get_field("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $person_id);
}

/**
 * Checks of request for need to update the staff image.
 *
 * @return bool
 */
function fn_staff_need_image_update()
{
    if (!empty($_REQUEST['file_staff_main_image_icon']) && is_array($_REQUEST['file_staff_main_image_icon'])) {
        $image_banner = reset($_REQUEST['file_staff_main_image_icon']);
        if ($image_banner == 'staff_main') {
            return false;
        }
    }
    return true;
}

function fn_staff_image_all_links($person_id, $pair_data, $main_lang_code = DESCR_SL)
{
    if (!empty($pair_data)) {
        $pair_id = reset($pair_data);
        $_staff_image_id = db_query("INSERT INTO ?:staff_images (staff_id) VALUE(?i)", $person_id);
        fn_add_image_link($_staff_image_id, $pair_id);
    }
}
