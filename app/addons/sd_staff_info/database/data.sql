REPLACE INTO ?:staff (`first_name`, `last_name`, `email`, `pos`, `status`) VALUES('John', 'Doe', 'john.doe@gmail.com', '1', 'A');
REPLACE INTO ?:staff (`first_name`, `last_name`, `email`, `pos`, `status`) VALUES('Forest', 'Gump', 'run.forest@gmail.com', '2', 'A');
REPLACE INTO ?:staff (`first_name`, `last_name`, `email`, `pos`, `status`) VALUES('Fenimor', 'Cuper', 'info.fc@gmail.com', '3', 'A');
REPLACE INTO ?:staff_data (`staff_id`, `title`, `description`, `lang_code`) VALUES('1', 'director', 'some text', 'en');
REPLACE INTO ?:staff_data (`staff_id`, `title`, `description`, `lang_code`) VALUES('2', 'deliveryman', 'Neck for Head of family', 'en');
REPLACE INTO ?:staff_data (`staff_id`, `title`, `description`, `lang_code`) VALUES('3', 'designer', 'alone', 'en');

