
{if $person}
    {assign var="id" value=$person.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{capture name="mainbox"}
<form action="{""|fn_url}" method="post" class="form-horizontal form-edit" name="staff_update_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="person_id" value="{$id}" />
  {capture name="tabsbox"}
    <div id="content_general">
        <div class="control-group">
            <label for="elm_person_first_name" class="control-label cm-required">{__("first_name")}</label>
            <div class="controls">
            <input type="text" name="person_data[first_name]" id="elm_person_first_name" value="{$person.first_name}" size="25" class="input-large" /></div>
        </div>
        <div class="control-group">
            <label for="elm_person_last_name" class="control-label">{__("last_name")}</label>
            <div class="controls">
            <input type="text" name="person_data[last_name]" id="elm_person_last_name" value="{$person.last_name}" size="25" class="input-large" /></div>
        </div>
        <div class="control-group">
            <label for="elm_person_title" class="control-label">{__("function_title")}</label>
            <div class="controls">
            <input type="text" name="person_data[title]" id="elm_person_title" value="{$person.title}" size="25" class="input-large" /></div>
        </div>
        <div class="control-group">
            <label for="elm_person_email" class="control-label">{__("email")}</label>
            <div class="controls">
                <input type="text" name="person_data[email]" id="elm_person_email" value="{$person.email|default:""}" size="3"/>
            </div>
        </div>
        <div class="control-group" id="person_text">
            <label class="control-label" for="elm_person_description">{__("description")}:</label>
            <div class="controls">
                <textarea id="elm_person_description" name="person_data[description]" cols="35" rows="8" class="cm-wysiwyg input-large">{$person.description}</textarea>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_person_position" class="control-label">{__("staff_position")}</label>
            <div class="controls">
                <input type="text" name="person_data[pos]" id="elm_person_position" value="{$person.pos|default:"0"}" size="3"/>
            </div>
        </div>
    </div>

    <div class="control-group" id="staff_graphic">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl"
                image_name="staff_main"
                image_object_type="foto"
                image_pair=$person.main_pair
                image_object_id=$id
                no_detailed=true
                hide_titles=true
            }
        </div>
    </div>
  
    {include file="common/select_status.tpl" input_name="person_data[status]" id="elm_person_status" obj=$person}

    {/capture}



    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name="staff_list" active_tab=$smarty.request.selected_section track=true}
</form>
{/capture}

{** Form submit section **}
{capture name="buttons"}
{include file="buttons/save_cancel.tpl" but_name="dispatch[staff_info.update]" but_role="submit-link" but_target_form="staff_update_form" save=$id}
{/capture}
{** /Form submit section **}

{if $person}
    {$title_start = __("editing_person")}
    {$title_end = $person.first_name}
{else}
    {$_title=__("add_person")}
{/if}

{include file="common/mainbox.tpl" title_start=$title_start title_end=$title_end title=$_title content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}

