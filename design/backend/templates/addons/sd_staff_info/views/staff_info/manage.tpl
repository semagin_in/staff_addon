{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="manage_datafeeds_form">

{if $staff}
<div class="table-responsive-wrapper">
    <table class="table sortable table-middle table--relative table-responsive">
    <thead>
        <tr>
            <th width="45%" class="nowrap">{__("staff_position")}</th>
            <th width="35%" class="nowrap">{__("person_name")}</th>
            <th width="1%" class="nowrap">&nbsp;</th>
            <th width="5%" class="nowrap right">{__("staff_status")}</th>
        </tr>
    </thead>
    {foreach from=$staff item=person}
    <tr class="cm-row-status-{$person.status|lower}">
        <td class="nowrap" data-th="{__("pos")}">
            {$person.pos}
        </td>
        <td data-th="{__("name")}">
            <a href="{"staff_info.update?person_id=`$person.staff_id`"|fn_url}">{$person.first_name} {$person.last_name}</a>
        </td>
        <td class="nowrap" data-th="{__("tools")}">
            {capture name="tools_list"}
                <li>{btn type="list" text=__("edit") href="staff_info.update?person_id=`$person.staff_id`"}</li>
                <li>{btn type="list" class="cm-confirm" text=__("delete") href="staff_info.delete?person_id=`$person.staff_id`" method="POST"}</li>

            {/capture}
            <div class="hidden-tools">
                {dropdown content=$smarty.capture.tools_list}
            </div>
        </td>
        <td class="right" data-th="{__("status")}">
            {include file="common/select_popup.tpl" id=$person.staff_id status=$person.status object_id_name="staff_id" table="staff" popup_additional_class="`$no_hide_input` dropleft"}
        </td>
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {if $persons}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="manage_staff_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list class="mobile-hide"}
    {/if}
{/capture}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff_info.add" prefix="bottom" title="{__("add_staff")}" hide_tools=true icon="icon-plus"}
{/capture}

</form>

{/capture}

{include file="common/mainbox.tpl" title=__("staff_info") content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}