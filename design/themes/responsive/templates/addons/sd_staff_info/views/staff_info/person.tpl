{assign var="title" value=__("sd_staff_info")}
{if $person}
    {assign var="id" value=$person.staff_id}
{else}
    {assign var="id" value=0}
{/if}
{if $person}
    <div class="ty-feature">
        {if $person.main_pair}
            <div class="ty-feature__image">
                {include file="common/image.tpl"
                    show_detailed_link=false
                    images=$person.main_pair
                    no_ids=true
                    image_id="person_image"
                    image_auto_size=true
                    image_height="140"
                }
            </div>
        {/if}
        <div class="ty-feature__description ty-wysiwyg-content">
            {$person.first_name nofilter} {$person.last_name nofilter}
        </div>
        <div class="ty-feature__description ty-wysiwyg-content">
            {__("function_title")}: {$person.title nofilter}
        </div>
        <div class="ty-feature__description ty-wysiwyg-content">
            {__("email")}: <a class="row-status" href="mailto:{$person.email|escape:url}">{$person.email}</a>
        </div>
        <div class="ty-feature__description ty-wysiwyg-content">
            {__("description")}: {$person.description nofilter}
        </div>
    </div>
{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}
{capture name="mainbox_title"}{$title}{/capture}
  

