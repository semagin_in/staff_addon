{assign var="title" value=__("sd_staff_info")}

{if $staff}
	<div class="ty-mainbox-body">
	{foreach from=$staff item=person key=key name="staff"}
		<div class="ty-companies">
		    {if $person.main_pair}
	            <div class="ty-feature__image">
	                {include file="common/image.tpl"
	                    show_detailed_link=false
	                    images=$person.main_pair
	                    no_ids=true
	                    image_id="person_image"
	                    image_auto_size=true
	                    image_height="40"
	                }
	            </div>
        	{/if}
		    <div class="ty-staff__info">
		    	<a href="{"staff_info.person?person_id=`$person.staff_id`"|fn_url}">{$person.first_name} {$person.last_name}</a>
		        <div>
		            {$person.title nofilter}
		        </div>
		    </div>
		</div>
	{/foreach}
	</div>
{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}

{capture name="mainbox_title"}{$title}{/capture}
